﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Device.Location;
using System;
using System.Text.RegularExpressions;

namespace PAED
{
    class Ordenacio
    {
        struct Element
        {
            public Post p;
            public GeoCoordinate location;
        }
        struct Besties
        {
            public String username;
            public long closeness;
        }

        private static List<Element> convertMyGeoLocation(Post[] p)
        {
            List<Element> list = new List<Element>();
            Element e = new Element();
            for (int i = 0; i < p.Length; i++)
            {
                e.p = p[i];
                e.location = Post.ConvertToGeoLocation(p[i].location[0], p[i].location[1]);

                list.Add(e);
            }
            return list;
        }

        // Displays posts ordered by the publish date
        public static void displayPublished (Post[] p)
        {
            for (int i = 0; i < p.Length; i++)
            {
                Console.Write("Id: " + p[i].id + " - Published: " + p[i].published + " - Location: [" + p[i].location[0] + "," + p[i].location[1] + "] - ");
                if (p[i].liked_by.Count > 1) { Console.Write("Liked by: " + p[i].liked_by.Count + " people - "); }
                else { Console.Write("Liked by: " + p[i].liked_by.Count + " person"); }
                if (p[i].commented_by.Count > 1) { Console.Write("Commented by: " + p[i].commented_by.Count + " people\n"); }
                else { Console.Write("Commented by: " + p[i].commented_by.Count + " person\n"); }
            }
        }

        //Selection Sort - Published
        public static void SelectionSort(Post[] p)
        {
            // This takes a few seconds to execute
            int length = p.Length;
            int smallestIndex, index, minIndex;
            for (index = 0; index < length - 1; index++)
            {
                smallestIndex = index;
                for (minIndex = index + 1; minIndex < length; minIndex++)
                {
                    if (p[minIndex].published > p[smallestIndex].published) { smallestIndex = minIndex; }
                }
                Swap(ref p[smallestIndex], ref p[index]);
            }
            Ordenacio.displayPublished(p);
        }

        //Selection Sort - Location
        public static void SelectionSort(Post[] p, GeoCoordinate reference)
        {
            List<Element> post = Ordenacio.convertMyGeoLocation(p);
            Element eSort = new Element();
            
            int length = p.Length;
            int smallestIndex, index, minIndex;

            for (index = 0; index < length - 1; index++)
            {
                smallestIndex = index;

                for (minIndex = index + 1; minIndex < length; minIndex++)
                {
                    if (reference.GetDistanceTo(post[minIndex].location) < reference.GetDistanceTo(post[smallestIndex].location)) { smallestIndex = minIndex; }
                }
                eSort = post[smallestIndex];
                post[smallestIndex] = post[index];
                post[index] = eSort;
            }
            Ordenacio.displayLocation(post);
        }

        private static void Swap(ref Post x, ref Post y)
        {
            Post temp = x;
            x = y;
            y = temp;
        }

        //This doesn't work because of StackOverFlowException
        public static void SelectionSortRecursive(Post[] p, int startIndex)
        {
            int maxIndex = startIndex;

            if (startIndex < p.Length)
            {
                for (int index = startIndex + 1; index < p.Length; index++)
                {
                    if (p[index].published > p[maxIndex].published) { maxIndex = index; }
                }
                Swap(ref p[startIndex], ref p[maxIndex]);
            }
        }


        // Quick Sort - Published
        public static void Quicksort(Post[] posts, int left, int right)
        {
            int i = left, j = right;
            Post pivot = posts[(left + right) / 2];

            while (i <= j)
            {
                while (posts[i].published > pivot.published) { i++; }
                while (posts[j].published < pivot.published) { j--; }

                if (i < j)
                {
                    // Swap the two posts
                    Swap(ref posts[i], ref posts[j]);
                    i++;
                    j--;
                }
                else { if (i == j) { i++; j--; } }
            }
            // Recursive calls of the algorithm
            if (left < j) { Quicksort(posts, left, j); }
            if (i < right) { Quicksort(posts, i, right); }
        }

        private static void DoQuickSort (List<Element> list, int left, int right, GeoCoordinate reference)
        {
            int i = left, j = right;
            double dist_pivotPost;
            Element e = list[(left + right) / 2];
            GeoCoordinate geo_post;
            
            double dist_pivotReference = reference.GetDistanceTo(e.location);
            while (i <= j)
            {
                geo_post = list[i].location;
                dist_pivotPost = geo_post.GetDistanceTo(reference);
                while (dist_pivotPost < dist_pivotReference)
                {
                    i++;
                    geo_post = list[i].location;
                    dist_pivotPost = geo_post.GetDistanceTo(reference);
                }

                geo_post = list[j].location;
                dist_pivotPost = geo_post.GetDistanceTo(reference);
                while (dist_pivotPost > dist_pivotReference)
                {
                    j--;
                    geo_post = list[j].location;
                    dist_pivotPost = geo_post.GetDistanceTo(reference);
                }

                if (i < j)
                {
                    // Swap
                    Element tmp = list[i];
                    list[i] = list[j];
                    list[j] = tmp;
                    i++;
                    j--;
                }
                else { if (i == j) { i++; j--; } }
            }
            // Recursive calls
            if (left < j) { DoQuickSort(list, left, j, reference); }
            if (i < right) { DoQuickSort(list, i, right, reference); }
        }

        //Quicksort segons la ubicació del Post
        public static void Quicksort(Post[] posts, int left, int right, GeoCoordinate reference)
        {
            List<Element> list = Ordenacio.convertMyGeoLocation(posts);
            Ordenacio.DoQuickSort(list, left, right, reference);
            Ordenacio.displayLocation(list);
        }

        /*Mostrar por pantalla los métodos por Location*/
        private static void displayLocation (List<Element> l) {

            for (int i = 0; i < l.Count; i++)
            {
                Console.Write("Id: " + l[i].p.id + " - Published: " + l[i].p.published + " - Location: [" + l[i].p.location[0] + "," + l[i].p.location[1] + "] - ");
                if (l[i].p.liked_by.Count > 1) { Console.Write("Liked by: " + l[i].p.liked_by.Count + " people - "); }
                else { Console.Write("Liked by: " + l[i].p.liked_by.Count + " person"); }
                if (l[i].p.commented_by.Count > 1) { Console.Write("Commented by: " + l[i].p.commented_by.Count + " people\n"); }
                else { Console.Write("Commented by: " + l[i].p.commented_by.Count + " person\n"); }
            }
        }

        // Merge Sort - ID Posts
        public static void DoMergeId(Post[] p, int left, int mid, int right)
        {
            Post[] tmp = new Post[p.Length];
            int i, leftEnd, numElements, tmp_pos;
            leftEnd = (mid - 1);
            tmp_pos = left;
            numElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (p[left].id <= p[mid].id) { tmp[tmp_pos++] = p[left++]; }
                else { tmp[tmp_pos++] = p[mid++]; }
            }
            while (left <= leftEnd) { tmp[tmp_pos++] = p[left++]; }
            while (mid <= right) { tmp[tmp_pos++] = p[mid++]; }
            for (i = 0; i < numElements; i++) { p[right] = tmp[right]; right--; }

        }
        public static void MergeSortRecursiveId(Post[] p, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSortRecursiveId(p, left, mid);
                MergeSortRecursiveId(p, (mid + 1), right);

                DoMergeId(p, left, (mid + 1), right);
            }
        }
        // Merge Sort - Published
        public static void DoMerge (Post[] p, int left, int mid, int right)
        {
            Post[] tmp = new Post[p.Length];
            int i, leftEnd, numElements, tmp_pos;
            leftEnd = (mid - 1);
            tmp_pos = left;
            numElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (p[left].published >= p[mid].published) { tmp[tmp_pos++] = p[left++]; }
                else { tmp[tmp_pos++] = p[mid++]; }
            }
            while (left <= leftEnd) { tmp[tmp_pos++] = p[left++]; }
            while (mid <= right) { tmp[tmp_pos++] = p[mid++]; }
            for (i = 0; i < numElements; i++) { p[right] = tmp[right]; right--; } 

        }
        public static void MergeSortRecursive (Post[] p, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSortRecursive(p, left, mid);
                MergeSortRecursive(p, (mid + 1), right);

                DoMerge(p, left, (mid + 1), right);
            }
        }

        // Merge Sort - Location
        
        private static void DoMerge (Element[] list, GeoCoordinate reference, int left, int mid, int right)
        {
            Element[] tmp = new Element[list.Length];
            int i, leftEnd, numElements, tmp_pos;
            leftEnd = (mid - 1);
            tmp_pos = left;
            numElements = (right - left + 1);
            while ((left <= leftEnd) && (mid <= right))
            {
                if (reference.GetDistanceTo(list[left].location) <= reference.GetDistanceTo(list[mid].location)) { tmp[tmp_pos++] = list[left++]; }
                else { tmp[tmp_pos++] = list[mid++]; }
            }
            while (left <= leftEnd) { tmp[tmp_pos++] = list[left++]; }
            while (mid <= right) { tmp[tmp_pos++] = list[mid++]; }
            for (i = 0; i < numElements; i++) { list[right] = tmp[right]; right--; }
        }
        private static void MergeSort (Element[] list, GeoCoordinate reference, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSort(list, reference, left, mid);
                MergeSort(list, reference, (mid + 1), right);
                DoMerge(list, reference, left, (mid + 1), right);
            }
        }
        public static void MergeSortRecursive (Post[] p, GeoCoordinate reference, int left, int right)
        {
            List<Element> post = Ordenacio.convertMyGeoLocation(p);
            Element[] aux = post.ToArray();
            Ordenacio.MergeSort(aux,reference, left, right);

            for (int i = 0; i < aux.Length; i++)
            {
                Console.Write("Id: " + aux[i].p.id + " - Published: " + aux[i].p.published + " - Location: [" + aux[i].p.location[0] + "," + aux[i].p.location[1] + "] - ");
                if (aux[i].p.liked_by.Count > 1) { Console.Write("Liked by: " + aux[i].p.liked_by.Count + " people - "); }
                else { Console.Write("Liked by: " + aux[i].p.liked_by.Count + " person"); }
                if (aux[i].p.commented_by.Count > 1) { Console.Write("Commented by: " + aux[i].p.commented_by.Count + " people\n"); }
                else { Console.Write("Commented by: " + aux[i].p.commented_by.Count + " person\n"); }
            }
        }

        // Radix sort segons temporalitat
        public static Post[] RadixSort(Post[] array)
        {
            bool done = false;
            int digitPosition = 0;

            // Create a list for each digit and for each digit a queue of all values of published
            List<Queue<Post>> buckets = new List<Queue<Post>>();
            InitBuckets(buckets);

            while (!done)
            {
                done = true;

                foreach (Post value in array)
                {
                    // Calculate the bucket number to know the digit of the element in the given position
                    int bucketNumber = GetBucketNumber(value.published, digitPosition);
                    if (bucketNumber > 0)
                    {
                        done = false;
                    }

                    // Add to the queue for that specific bucket
                    buckets[bucketNumber].Enqueue(value);
                }

                int i = 0;
                foreach (Queue<Post> bucket in buckets)
                {
                    while (bucket.Count > 0)
                    {
                        // Put everything back in the array
                        array[i] = bucket.Dequeue();
                        i++;
                    }
                }

                // Look at next digit position, next significant bit
                digitPosition++;
            }
            int length = array.Length;
            for (int j = 0; j < length / 2; j++)
            {
                Swap(ref array[j], ref array[array.Length - j - 1]);
            }
            Ordenacio.displayPublished(array);
            return array;
        }

        // Sorting the value (published) into buckets according to the digit position
        // Sorting the value (published) into buckets according to the digit position
        private static int GetBucketNumber(long value, int digitPosition)
        {
            long bucketNumber = value / (long)Math.Pow(10, digitPosition);
            bucketNumber = bucketNumber % 10;
            //int bucketNumber;
            //bucketNumber = (int)value;

            //int bucketNumber = (int)(value / (int)Math.Pow(10, digitPosition)) % 10;
            //if (bucketNumber == -3)
            //{
            //    bucketNumber = bucketNumber;
            //}
            return (int)bucketNumber;
        }

        // Create the queue for each bucket or each digit
        private static void InitBuckets(List<Queue<Post>> buckets)
        {
            for (int i = 0; i < 10; i++)
            {
                Queue<Post> q = new Queue<Post>();
                buckets.Add(q);
            }
        }

        //Radix sort segons ubicació
        public static Post[] RadixSort(Post[] array, GeoCoordinate reference)
        {
            List<Element> list = Ordenacio.convertMyGeoLocation(array);
            //Element[] aux = post.ToArray();

            // temporary array and the array of converted doubles to longs
            long[] t = new long[array.Length];
            long[] a = new long[array.Length];
            Post[] p_temp = new Post[array.Length];
            Post[] p_conv = new Post[array.Length];
            int lengthArray = array.Length;

            for (int i = 0; i < lengthArray; i++)
            {
                GeoCoordinate geo_post = list[i].location;
                double dist_postReference = geo_post.GetDistanceTo(reference);

                // Convert double to long
                a[i] = BitConverter.ToInt64(BitConverter.GetBytes(dist_postReference), 0);
                p_conv[i] = array[i];
            }

            int setLength = 4;
            int numBits = 64;
            long[] counter = new long[1 << setLength];
            long[] prefixes = new long[1 << setLength];
            int groups = numBits / setLength;
            int mask = (1 << setLength) - 1;
            int negativeNums = 0, positiveNums = 0;

            for (int c = 0, shift = 0; c < groups; c++, shift += setLength)
            {
                // initialize array of counter 
                for (int j = 0; j < counter.Length; j++)
                    counter[j] = 0;
                
                for (int i = 0; i < a.Length; i++)
                {
                    counter[(a[i] >> shift) & mask]++;

                    // count negative numbers
                    if (c == 0 && a[i] < 0)
                        negativeNums++;
                }
                if (c == 0) positiveNums = a.Length - negativeNums;

                // calculate the prefixes
                prefixes[0] = 0;
                for (int i = 1; i < counter.Length; i++)
                    prefixes[i] = prefixes[i - 1] + counter[i - 1];
                
                for (int i = 0; i < a.Length; i++)
                {
                    // Get the right index to sort the number in
                    long indice = prefixes[(a[i] >> shift) & mask]++;

                    if (c == groups - 1)
                    {
                        // Most significant btis. In case it is negative, sort inversely:
                        // put positives behind negatives
                        if (a[i] < 0)
                            indice = positiveNums - (indice - negativeNums) - 1;
                        else
                            indice += negativeNums;
                    }
                    t[indice] = a[i];
                    p_temp[indice] = p_conv[i];
                }
                

                t.CopyTo(a, 0);
                p_temp.CopyTo(p_conv, 0);
            }

            for (int i = 0; i < p_conv.Length; i++)
            {
                Console.Write("Id: " + p_conv[i].id + " - Published: " + p_conv[i].published + " - Location: [" + p_conv[i].location[0] + "," + p_conv[i].location[1] + "] - ");
                if (p_conv[i].liked_by.Count > 1) { Console.Write("Liked by: " + p_conv[i].liked_by.Count + " people - "); }
                else { Console.Write("Liked by: " + p_conv[i].liked_by.Count + " person"); }
                if (p_conv[i].commented_by.Count > 1) { Console.Write("Commented by: " + p_conv[i].commented_by.Count + " people\n"); }
                else { Console.Write("Commented by: " + p_conv[i].commented_by.Count + " person\n"); }
            }
            return p_conv;
        }


        // PRIORITATS
        
        public static Category[] tableCategories()
        {
            Category[] c = new Category[11];
            for (int i = 0; i < 11; i++) { c[i] = new Category(); }
            c[0].setName("landscape");
            c[1].setName("food");
            c[2].setName("sports");
            c[3].setName("style");
            c[4].setName("animals");
            c[5].setName("tv_shows");
            c[6].setName("fitness");
            c[7].setName("science_tech");
            c[8].setName("music");
            c[9].setName("travel");
            c[10].setName("architecture");

            return c;
        }

        public static long[] getLikehood(List<Usuari> instagram, int index, int likes, int comments)
        {
            long[] likehood = new long[likes + comments];
            for (int a = 0; a < likes; a++)
            {
                likehood[a] = instagram[index].likedPosts[a];
                if (a + 1 == likes)
                {
                    for (int b = a; b < comments; a++)
                    {
                        likehood[a] = instagram[index].commentedPosts[a];
                    }
                }
            }
            long[] aux = likehood.Distinct().ToArray();
            Ordenacio.MergeSortLikehood(aux, 0, aux.Length - 1);
            return aux;
        }

        public static Category[] topCategories(long[] likehood, Post[] arrayPosts)
        {
            Category[] c = Ordenacio.tableCategories();
            for (int a = 0; a < likehood.Length; a++)
            {
                for (int b = 0; b < c.Length; b++)
                {
                    long idPost = likehood[a];
                    if (arrayPosts[idPost].category == c[b].name) { c[b].total++; }
                }
            }
            Ordenacio.SelectionSortCategory(c);
            return c;
        }

        private static Post[] concatFeed (Post[] source, Post[] dest)
        {
            Post[] z = source.Concat(dest).ToArray();
            return z;
        }

        private static Post[] sortingFeed(List<Usuari> instagram, Besties[] b, Category[] c)
        {
            //Para cada categoria
            Post[] feed = new Post[0];

            for (int i = 0; i < c.Length && c[i].total > 0; i++)
            {
                string hello = c[i].name;
                //Para cada conexión que tiene que con un usuario
                for (int a = 0; a < b.Length; a++)
                {
                    string user = b[a].username;
                    string str = Regex.Match(user, @"\d+").Value;
                    int num = Convert.ToInt32(str);
                    int totalP = instagram[num].posts.Count();
                    ArrayList filter = new ArrayList();

                    //De todos sus posts cogemos de esta categoria
                    Post[] posts = new Post[totalP];
                    for (int j = 0; j < totalP; j++) { posts[j] = instagram[num].posts[j]; }
                    foreach (Post p in posts)
                    {
                        if (p.category.Contains(hello))
                        {
                            filter.Add(p);
                        }
                    }
                    Post[] filtering = filter.ToArray(typeof(Post)) as Post[];
                    //ordenar por published
                    Ordenacio.MergeSortRecursive(filtering,0,filtering.Length-1);
                    //hacer concatenate en un array
                    feed = Ordenacio.concatFeed(feed, filtering);
                }
            }
            return feed;
        }

        public static void displayPriority (Post[] p)
        {
            for (int i = 0; i < p.Length; i++)
            {
                Console.Write("Id: " + p[i].id + " - Published: " + p[i].published + " - Location: [" + p[i].location[0] + "," + p[i].location[1] + "] - " + "Category: " + p[i].category + " - ");
                if (p[i].liked_by.Count > 1) { Console.Write("Liked by: " + p[i].liked_by.Count + " people - "); }
                else { Console.Write("Liked by: " + p[i].liked_by.Count + " person"); }
                if (p[i].commented_by.Count > 1) { Console.Write("Commented by: " + p[i].commented_by.Count + " people\n"); }
                else { Console.Write("Commented by: " + p[i].commented_by.Count + " person\n"); }
            }
        }

        public static void priorityFeed (List<Usuari> instagram, Post[] arrayPosts, int num)
        {
            Post[] feed = new Post[0];
            int likes = instagram[num].likedPosts.Count();
            int comments = instagram[num].commentedPosts.Count();

            long[] likehood = Ordenacio.getLikehood(instagram, num, likes, comments);
            Ordenacio.MergeSortRecursiveId(arrayPosts, 0, arrayPosts.Length - 1);
            Category[] c = Ordenacio.topCategories(likehood, arrayPosts);

            long numConn = instagram[num].connections.Count();
            Besties[] conn = new Besties[numConn];
            for (int a = 0; a < numConn; a++)
            {
                conn[a].username = instagram[num].connections[a].username;
                conn[a].closeness = instagram[num].connections[a].visits + instagram[num].connections[a].likes + instagram[num].connections[a].comments;
            }
            Ordenacio.MergeSortRecursiveConn(conn, 0, conn.Length - 1);
            feed = Ordenacio.sortingFeed(instagram, conn, c);
            Ordenacio.displayPriority(feed);
        }

        private static void DoMergeConn(Besties[] p, int left, int mid, int right)
        {
            Besties[] tmp = new Besties[p.Length];
            int i, leftEnd, numElements, tmp_pos;
            leftEnd = (mid - 1);
            tmp_pos = left;
            numElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (p[left].closeness >= p[mid].closeness) { tmp[tmp_pos++] = p[left++]; }
                else { tmp[tmp_pos++] = p[mid++]; }
            }
            while (left <= leftEnd) { tmp[tmp_pos++] = p[left++]; }
            while (mid <= right) { tmp[tmp_pos++] = p[mid++]; }
            for (i = 0; i < numElements; i++) { p[right] = tmp[right]; right--; }

        }
        private static void MergeSortRecursiveConn(Besties[] p, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSortRecursiveConn(p, left, mid);
                MergeSortRecursiveConn(p, (mid + 1), right);

                DoMergeConn(p, left, (mid + 1), right);
            }
        }

        private static void SwapCategory(ref Category x, ref Category y)
        {
            Category temp = x;
            x = y;
            y = temp;
        }

        public static void SelectionSortCategory(Category[] p)
        {
            int length = p.Length;
            int smallestIndex, index, minIndex;
            for (index = 0; index < length - 1; index++)
            {
                smallestIndex = index;
                for (minIndex = index + 1; minIndex < length; minIndex++)
                {
                    if (p[minIndex].total > p[smallestIndex].total) { smallestIndex = minIndex; }
                }
                SwapCategory(ref p[smallestIndex], ref p[index]);
            }
        }
        public static void DoMerge(long[] p, int left, int mid, int right)
        {
            long[] tmp = new long[p.Length];
            int i, leftEnd, numElements, tmp_pos;
            leftEnd = (mid - 1);
            tmp_pos = left;
            numElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (p[left] <= p[mid]) { tmp[tmp_pos++] = p[left++]; }
                else { tmp[tmp_pos++] = p[mid++]; }
            }
            while (left <= leftEnd) { tmp[tmp_pos++] = p[left++]; }
            while (mid <= right) { tmp[tmp_pos++] = p[mid++]; }
            for (i = 0; i < numElements; i++) { p[right] = tmp[right]; right--; }

        }
        public static void MergeSortLikehood(long[] p, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                MergeSortLikehood(p, left, mid);
                MergeSortLikehood(p, (mid + 1), right);

                DoMerge(p, left, (mid + 1), right);
            }
        }
    }
}
