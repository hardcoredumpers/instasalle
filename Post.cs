﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAED
{
    public class Post
    {
        public long id { get; set; }
        public long published { get; set; }
        public double[] location { get; set; }
        public string category { get; set; }
        public List<string> liked_by { get; set; }
        public List<string> commented_by { get; set; }
        
        public static GeoCoordinate ConvertToGeoLocation (double latitude, double longitude)
        {
            return new GeoCoordinate(latitude, longitude);
        }

    }
}
