#InstaSalle
Este proyecto se trata de un programa que ordena diferentes posts de diferentes usuarios en InstaSalle, dado ficheros JSON de diferentes tamaños. Está implementado en C#. Es recomendable el uso de Visual Studio para ver su funcionamiento.

##Cómo funciona
1. Abra el proyecto en Visual Studio.
2. En la pestaña Debug, haga clic en Properties.
3. En la sección de "Start options", escribe los parámetros en "Command line arguments". Cada argumento debe estar entre comillas dobles y separados por un espacio en blanco.
4. Escriba el nombre del fichero JSON como primer parámetro de la línea de comandas.
5. Escriba el criterio de la ordenación como siguiente parámetro: temporality, location o priorities
6. Si se trata de location, especifique la latitud y la longitud como parámetros separados.
7. Si se trata de priorities, escriba el nombre del usuario existente como siguiente parámetro.
8. Por último, en caso de temporality y location, escriba el método de ordenación deseado: quick, selection, merge, radix
9. Haga clic en el botón Start.

##Ejemplos de uso
```sh
"priorities.json" "priorities" "user20"
"s_dataset.json" "location" "35.005647234" "17.015637654" "radix"
"xs_dataset.json" "temporality" "selection"
```

###Funcionalidades
- Selection sort
- Quicksort
- Merge sort
- Radix sort

###Funcionalidades adicionales
- **Algoritmo de ordenación según las prioridades (opción 3)**: No se trata de ningún método de ordenación en concreto.