﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Device.Location;
using System;
using System.Text.RegularExpressions;


namespace PAED
{
    public class Menu {
        

        public static void displayFunctionalities (int n, ref string args1)
        {
            switch (n)
            {
                case 1:
                    Console.WriteLine("\n---------- TEMPORALITY\n");
                    args1 = "temporality";
                    break;
                case 2:
                    Console.WriteLine("\n---------- LOCATION\n");
                    args1 = "location";
                    break;
                case 3:
                    Console.WriteLine("\n---------- PRIORITIES\n");
                    args1 = "priorities";
                    break;
            }
        }

        public static void displaySortingMethods (string n, ref string args2)
        {
            switch (n)
            {
                case "a": args2 = "quick";
                    break;
                case "b": args2 = "selection";
                    break;
                case "c": args2 = "merge";
                    break;
                case "d": args2 = "radix";
                    break;
            }
        }

        public static void doYourThing(String jsonFile, String functionality, String method, String latitude, String longitude, String user)
        {
            //Read JSON
            ArrayList arrayList = new ArrayList();
            using (StreamReader r = new StreamReader(jsonFile))
            {
                string json = r.ReadToEnd();
                var instagram = JsonConvert.DeserializeObject<List<Usuari>>(json);
                arrayList = new ArrayList();

                // Put each post in the ArrayList
                for (int i = 0; i < instagram.Count(); i++)
                {
                    for (int j = 0; j < instagram[i].posts.Count(); j++)
                    {
                        arrayList.Add(instagram[i].posts[j]);
                    }
                }

                //Converting this arrayList to array of Posts
                Post[] arrayPosts = arrayList.ToArray(typeof(Post)) as Post[];

                switch (functionality)
                {
                    case "temporality":
                        switch (method)
                        {
                            case "quick":
                                Ordenacio.Quicksort(arrayPosts, 0, arrayPosts.Length - 1);
                                Ordenacio.displayPublished(arrayPosts);
                                break;
                            case "selection":
                                Ordenacio.SelectionSort(arrayPosts);

                                break;
                            case "merge":
                                Ordenacio.MergeSortRecursive(arrayPosts, 0, arrayPosts.Length - 1);
                                Ordenacio.displayPublished(arrayPosts);
                                break;
                            case "radix":
                                Ordenacio.RadixSort(arrayPosts);
                                break;
                        }
                        break;
                    case "location":
                        double lat, lon;
                        lat = Convert.ToDouble(latitude);
                        lon = Convert.ToDouble(longitude);
                        GeoCoordinate location = Post.ConvertToGeoLocation(lat, lon);

                        switch (method)
                        {
                            case "quick":
                                Ordenacio.Quicksort(arrayPosts, 0, arrayPosts.Length - 1, location);
                                break;
                            case "selection":
                                Ordenacio.SelectionSort(arrayPosts, location);
                                break;
                            case "merge":
                                Ordenacio.MergeSortRecursive(arrayPosts, location, 0, arrayPosts.Length - 1);
                                break;
                            case "radix":
                                Ordenacio.RadixSort(arrayPosts, location);
                                break;
                        }
                        break;
                    case "priorities":
                        /* 
                         * 1. Crear una tabla de categorias y tendrá otra columna con el número de posts
                         * 2. De esta tabla coger el max. published, el más reciente, saber qué categoria es
                         * 3. De sus conexiones, mirar el usuario con más follows, visits, likes
                         * 4. Para cada uno de sus conexiones coger los posts relacionados a sus categorias y ordenarlo por published
                         * 5. Todo lo demás se ordenará por published, por proximidad a los posts del usuario, likes visits y comments.                        
                         */
                        
                        string number = Regex.Match(user, @"\d+").Value;
                        int num = Convert.ToInt32(number);
                        Ordenacio.priorityFeed(instagram, arrayPosts, num);
                        break;
                }
            }
        }
    }
}
