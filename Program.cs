﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Device.Location;

namespace PAED
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string jsonFile = args[0];
            string functionality, method, latitude, longitude, user;
            latitude = "";
            longitude = "";
            user = "";

            functionality = args[1];
            method = args[2];
            
            if (functionality == "priorities") { user = args[2]; }
            if (functionality == "location") { latitude = args[3]; longitude = args[4]; }

            Menu.doYourThing(jsonFile, functionality, method, latitude, longitude, user);
            Console.Write("\n[Please Press Enter to Exit the Program]");
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            while (keyInfo.Key != ConsoleKey.Enter)
                keyInfo = Console.ReadKey();
            //Console.Write("");

        }
    }
}
