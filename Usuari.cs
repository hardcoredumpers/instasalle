﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PAED
{
    class Usuari
    {
        public string username { get; set; }
        public long followers { get; set; }
        public long follows { get; set; }
        public Connection[] connections { get; set; }
        public Post[] posts { get; set; }
        [JsonProperty(PropertyName = "likedPosts")]
        public List<long> likedPosts { get; set; }
        public List<long> commentedPosts { get; set; }
    }

}
